package methods;

import java.sql.Statement;
import java.sql.SQLException;
import dto.*;

public class createTable {
	public static void createTable(Table fabricantes, String creationQuery) {
		try {
			String querydb = "USE " + fabricantes.getNameDB() + ";";
			Statement stdb = openConnection.connection.createStatement();
			stdb.executeUpdate(querydb);
			
			String query = "CREATE TABLE " + fabricantes.getTableName() + creationQuery;
			Statement st = openConnection.connection.createStatement();
			st.executeUpdate(query);
			System.out.println("Table created!");
		}catch(SQLException e) {
			System.out.println("Error creating the new table: ");
			System.out.println(e.getMessage());
		}		
	}
}
