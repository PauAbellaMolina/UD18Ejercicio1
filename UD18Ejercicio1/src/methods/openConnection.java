package methods;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class openConnection {
	public static Connection connection;
	
	public static void openConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://192.168.0.170:3306?useTimezone=true&serverTimezone=UTC","xxxx", "xxxx");
			System.out.println("Connected!");
		}catch(SQLException | ClassNotFoundException e) {
			System.out.println("Error");
			System.out.println(e.getMessage());
		}
	}
}
