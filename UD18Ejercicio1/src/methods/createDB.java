package methods;

import java.sql.Statement;
import java.sql.SQLException;
import dto.Database;

public class createDB {
	public static void createDB(Database newDatabase) {
		try {
			String query = "CREATE DATABASE " + newDatabase.getNameDB();
			Statement st = openConnection.connection.createStatement();
			st.executeUpdate(query);
			System.out.println("Database created!");
		}catch(SQLException e) {
			System.out.println("Error creating the new database");
		}		
	}
}
