import dto.*;
import methods.*;

public class Ejercicio1App {
	public static void main(String[] args) {
		Database newDatabase = new Database("UD18Ejercicio1");
		Table Fabricantes = new Table("UD18Ejercicio1", "Fabricantes");
		Table Articulos = new Table("UD18Ejercicio1", "Articulos");
		openConnection.openConnection();
		createDB.createDB(newDatabase);
		createTable.createTable(Fabricantes, " (`Codigo` INT NOT NULL AUTO_INCREMENT, `Nombre` NVARCHAR(100) NULL, PRIMARY KEY (`Codigo`));");
		createTable.createTable(Articulos, " (`Codigo` INT NOT NULL AUTO_INCREMENT, `Nombre` NVARCHAR(100) NULL, `Precio` INT NULL, `Fabricante` INT NULL, PRIMARY KEY (`Codigo`), INDEX `Fabricante_idx` (`Fabricante` ASC) VISIBLE, CONSTRAINT `Fabricante` FOREIGN KEY (`Fabricante`) REFERENCES `UD18Ejercicio1`.`Fabricantes` (`Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION);");
		insertData.insertData("UD18Ejercicio1", "Fabricantes", "(Nombre) VALUE('Lenovo');");
		insertData.insertData("UD18Ejercicio1", "Fabricantes", "(Nombre) VALUE('Asus');");
		insertData.insertData("UD18Ejercicio1", "Fabricantes", "(Nombre) VALUE('Apple');");
		insertData.insertData("UD18Ejercicio1", "Fabricantes", "(Nombre) VALUE('Samsung');");
		insertData.insertData("UD18Ejercicio1", "Fabricantes", "(Nombre) VALUE('OnePlus');");
		insertData.insertData("UD18Ejercicio1", "Articulos", "(Nombre, Precio, Fabricante) VALUE('Thinkpad', 799, 1);");
		insertData.insertData("UD18Ejercicio1", "Articulos", "(Nombre, Precio, Fabricante) VALUE('RTX 2080Ti', 849, 2);");
		insertData.insertData("UD18Ejercicio1", "Articulos", "(Nombre, Precio, Fabricante) VALUE('iPhone 11 Max', 999, 3);");
		insertData.insertData("UD18Ejercicio1", "Articulos", "(Nombre, Precio, Fabricante) VALUE('Galaxy Note 20', 650, 4);");
		insertData.insertData("UD18Ejercicio1", "Articulos", "(Nombre, Precio, Fabricante) VALUE('Nord', 399, 5);");
		closeConnection.closeConnection();
	}
}
