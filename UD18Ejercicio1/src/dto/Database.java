package dto;

public class Database {
	protected String nameDB;
	
	public Database() {
		this.nameDB = "";
	}
	
	public Database(String nameDB) {
		this.nameDB = nameDB;
	}

	public String getNameDB() {
		return nameDB;
	}
}
